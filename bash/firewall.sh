#!/bin/bash
ports(){
	touch temp
	chmod 777 temp
sudo netstat -anltp | grep "LISTEN" | awk '{print($4)}' | sed 's|.*:::||' | sed 's|.*:||' > temp
i=1
while [ $i -le $(sed -n '$=' temp) ]
do
	echo "add rule: allow port: $(sed -n "${i} p" temp)"
	sudo ufw allow $(sed -n "${i} p" temp)
	let i=i+1
done
rm temp	
}
case $1 in

start)
ports
sudo ufw enable
;;
stop)
sudo ufw disable
;;
*)
echo "use start|stop"
;;
esac
exit