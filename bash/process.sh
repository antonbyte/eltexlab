#!/bin/sh

#Write a script that controls the execution of a process and, in the event of an interruption of this process, resumes its work.


checking(){	
x=`pidof $a` 
if [ "$x" > 0  ]
then
sleep 1m
else
firefox  
fi
}
create(){
ACT=$(whiptail --title "Создание процесса" --inputbox "Напишите имя процесса:" 10 60 3>&1 1>&2 2>&3)
CHAS=$(whiptail --title "Создание процесса" --inputbox "Введите часы:" 10 60 3>&1 1>&2 2>&3)
MIN=$(whiptail --title "Создание процесса" --inputbox "Введите минуты:" 10 60 3>&1 1>&2 2>&3)
DIS=`cat test | sed -n '/DISPLAY=:0/ p'`
LATTER="DISPLAY=:0"
if [ "$DIS" = "$LATTER" ]
then
echo "$MIN $CHAS * * * /home/anton/local/process.sh $ACT &" >> test
else
echo "DISPLAY=:0\n$MIN $CHAS * * * /home/anton/Eltex/Linux/bash/process.sh $ACT &" >> test
fi 
crontab test
}
edit(){
сrontab -r
sed -i '$d' test
ACTT=$(whiptail --title "Редактирование процесса" --inputbox "Напишите имя нового процесса:" 10 60 3>&1 1>&2 2>&3)
CHASS=$(whiptail --title "Редактирование процесса" --inputbox "Введите новые часы:" 10 60 3>&1 1>&2 2>&3)
MINN=$(whiptail --title "Редактирование процесса" --inputbox "Введите новые минуты:" 10 60 3>&1 1>&2 2>&3)
echo "$MINN $CHASS * * * /home/anton/Eltex/Linux/bash/process.sh $ACTT &"  >> test | crontab test
}
remove(){
MOD=`cat test | sed -n '/&/ p' | cat -n `
NUM=$(whiptail --title "Удаление процесса" --inputbox "\nВведите номер строки для удаления процесса:\n$MOD" 15 70 3>&1 1>&2 2>&3)
VAR=`cat test | sed -n '/&/ p' | sed "${NUM} !d"` 
echo "$VAR" >> test 
STR=`cat test | sort | uniq -u | sed '/DISPLAY=:0/d'` 
echo -e "DISPLAY=:0\n$STR" > test
crontab test 
}

a=$1
if [ -z "$a" ] 
then
OPTION=$(whiptail --title "Пользовательское меню" --menu "Процесс:" 10 60 3 \
"1" "Создать" \
"2" "Редактировать" \
"3" "Удалить" 3>&1 1>&2 2>&3 )
exitstatus=$OPTION
case $exitstatus in
1)
create
;; 
2)
edit
;;
3)
remove
;;
esac
else
checking
fi
exit

