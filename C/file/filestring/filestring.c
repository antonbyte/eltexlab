/*----------------------------------------
Исключить строки с длиной, больше заданной
Параметры командной строки: 
	1. Имя входного файла 
	2. Заданная длина строки
-----------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>   

void fileProcessing (FILE *file, FILE *outfile, char symbol, int j, int argument, char *buffer);
void freeMemory(FILE *file, FILE *outfile, char *buffer, char *bufferString);

int main(int argc, char *argv[]){
	if (argc < 4){
    printf("Нет входных параметров. Пример использования : %s <имя входного файла> <длина строки> <расширение выходного файла>\n", argv[0]);
    exit(0);
	}
   else
   	{  
       int argument = atoi(argv[2]) , j = 0;
   	   char symbol = 0, *buffer = NULL;
       FILE *file = fopen(argv[1], "r");    //открываю файл и связываю его с потоком (возврат адрес начала файла в памяти)
       if(file == NULL)
       {
       	 printf("Ошибка. Невозможно открыть файл.\n");
       	 exit(1);
       }      
       else
       {   
          char *tochaMas= strchr(argv[1], '.');     // возвращаю указатель на символ '.'
       	  int n = (tochaMas - argv[1]) + 1;    //  ((указатель точка - указатель нулевого элемнта) + точка) итог:filestring.
       	  char *bufferString = malloc(sizeof(char) * n);  //выделяю память под массиы filestring.
       	    for (int j = 0; j < n; j++){
       	      bufferString[j] = argv[1][j];         //собственно заполнение массива
       	     } 
       	     strcat(bufferString, argv[3]);         // копирую указатель на 3 параметр(строку) в новый указатель на строку
             FILE *outfile = fopen(bufferString, "w"); // открываю выходной файл для записи
             fileProcessing(file, outfile ,symbol, j, argument, buffer); 
             printf("Данные обработанны и записаны в файл %s\n", bufferString); 
             freeMemory(file, outfile, buffer, bufferString);  	   
       }
   	}
   	return 0;
 }




void fileProcessing (FILE *file, FILE *outfile, char symbol, int j, int argument, char *buffer){

 printf("Файл успешно открыт и связан с потоком\n");    	
 while((symbol=fgetc(file)) != EOF)     // читаю посимвольно с файла до тех пор пока не встречу конец файла(смещаю указатель)
    {
      if (symbol != '\n')               // каждый символ сравниваю с переходом на другую строку
      { 
      	j+=1;                           // считаю кол-во символов в строке
      }
      else
      {
      	if(j <= argument){                               //  сравниваю кол-во символов в строке с указанным кол-ом
        fseek( file , (-(j + 1)), SEEK_CUR );            // сдвигаю позицию указателя в файле на (-(j + 1)) бит (вообщем на начало строки)
      	buffer = malloc(sizeof(char) * j);               // выделяю память под буфер объемом равному длинне одной строки
        fread(buffer, sizeof(char), j + 1, file);         // читаю строку , длинной j + 1, каждый символ которой й байт в буфер
        if(ferror(file) != 0)
        {
        	printf("Ошибка чтения файла");
        	exit(1);
        }                                               
        fwrite(buffer, sizeof(char), j + 1, outfile);      // пишу строку из буфера в отдельный файл
        if(ferror(file) != 0)
        {
        	printf("Ошибка записи файла");
        	exit(1);
        }                                                  
       }
        j = 0; 	
      }
    }
    return ;     //возврат управления вызывающей функции
}  


void freeMemory(FILE *file, FILE *outfile, char *buffer, char *bufferString){
  fclose(file);
  fclose(outfile);
  free(buffer);
  free(bufferString);
  return ;
}
 
 
 	

       	    

       	   
 