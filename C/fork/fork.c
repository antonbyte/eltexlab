/*-----------------------------------------------------------------------------------
Умножение матрицы на вектор. Обработка одной строки матрицы - в порожденном процессе.
------------------------------------------------------------------------------------*/
#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <wait.h>

//интерфейсы программы
void fillMatrix(int**, int, int);
void fillVector(int*, int);
void PrintVector(int*, int);
int CalculateProcess(int**, int*, int, int);
void freeMemory(int**, int*, int);


int main(int argc, char const *argv[])
{
	if (argc < 3){
		printf("Нет входных параметров. Пример: %s < M - кол-во строк > < N - кол-во столбцов> \n", argv[0]);
	}
    else{
      int status, salat, result, count = 0;
      int M = atoi(argv[1]);  //конвертирую аргумент типа char в тип int 
      int N = atoi(argv[2]);  
      pid_t pid[M];  //тип данных под идентификатор процесса

      int *resultBuffer = (int*)malloc(sizeof(int) * N); //память под результирующий буффер вывода

      int **Matrix =(int**)malloc(sizeof(int*) * M); //выделяю память под массив строк в матрице

      for (int i = 0; i < M; i++)
      {
        Matrix[i] = (int*)malloc(sizeof(int) * N);   //выделяю память под массив столбцов в матрице
      }
        

      int *vector = (int*)malloc(sizeof(int) * N); // выделяю память под вектор

      fillMatrix(Matrix, M, N); //заполняю Матрицу случайными числами
      fillVector(vector, N);    //Заполняю Вектор случайными числами

        for (int i = 0; i < M; i++){
      	 pid[i] = fork();


      	 if (pid[i] == 0){   //ветка дочернего процесса
                result = CalculateProcess(Matrix, vector , N, i);    //тут то все умножение и происходит :)	 	   
      	 		printf("PIDchild%d:\n", getpid());
      	 		printf("A%d = %c", i, 124);
                for (int j = 0; j < N; j++){
                printf("%2d", Matrix[i][j]);
                }
                 printf("%2c\n", 124);
                 printf("B%d = %c", i, 124);
                 for (int i = 0; i < N; i++)
                 {
                 	printf("%2d", vector[i]);
                 }
                  printf("%2c\n", 124);
                  printf("C%d = A%d * B%d = %d\n", i, i, i, result);
      	 		exit(result);
      	 	}
      	      
      	 }

              do{
      	      for (int i = 0; i < M; i++){
      	        
                salat = waitpid(pid[i], &status, WUNTRACED | WCONTINUED);  //жду потомок завершит процесс
                if (salat == pid[i]){
                count++;
                resultBuffer[i] = WEXITSTATUS(status); //макросом возвращаю значение из потомка exit                                     
                }
            }
      	 	} while(count != M);
      	 
      	        freeMemory(Matrix, vector, M);  //очистка памяти
      	 		printf("\nPIDparent%d: Finishing...\nResulting vector: ", getpid());
                PrintVector(resultBuffer, N);   //печать результата
                free(resultBuffer);             //освобождаю буффер под результат

	return 0;

    }


}

//функция заполяет матрицу случ. числами
void fillMatrix(int **Matrix, int M, int N){
for (int i = 0; i < M ; i++) {
for (int j = 0; j < N ; j++) {
Matrix[i][j] = rand()%10;
    }
  }
  return ; //возврат управления в вызывающую функцию(можно не писать, если void, просто хороший тон)
}

//функция заполняет вектор случ. числами
void fillVector(int *vector, int N){
      for (int i = 0; i < N ; i++) {
vector[i] = rand()%10;
    }
    return ;
  }

//функция умножает строку на вектор и возвращает рузультат
int CalculateProcess(int **Matrix, int *vector , int N, int i){
int all = 0;
	for (int j = 0; j < N; j++){
		all += Matrix[i][j] * vector[j];
	}
    return all;
}

//функция печатает ркзультирующий вектор в терминале
void PrintVector(int *resultBuffer, int N){
printf("C = %c", 124);
for (int i = 0; i < N; i++){
printf("%5d", resultBuffer[i]);
    }
    printf("%2c\n", 124);
    return ;
  }

//функция освобождает память
void freeMemory(int **Matrix, int *vector, int M){
	for (int i = 0; i < M; i++)
	{
		free(Matrix[i]);
	}
    free(Matrix);
    free(vector);
    return ;
}

