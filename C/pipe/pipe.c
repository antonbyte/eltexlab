#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <wait.h>
#define MAX_LEN 20

//интерфесы взаимодействия
char** FillMass(unsigned short);
int ProcessingFiles(char**, unsigned short);
int TotalCheckSum(int*);
void freeMemory(char**);

unsigned short quantity;      //кол-во файлов
char **MassPointFile = NULL;  //указатель на указатель на строку
pid_t pidoff;                 //тут будет лежать идентификатор процесса
int CheckSum, status, count = 0;


//точка отсчета
int main(void)
{
	printf("\nEnter the quantity files:");
	scanf("%hi", &quantity);
	int fd[quantity][2]; //двумерный массив под файловые дискрипторы канала
	pid_t pid[quantity]; //тут будет лежать идентификатор процесса
	int *resultBuffer = malloc(sizeof(int) * quantity); //указатель на память, где будет лежать результат
	MassPointFile = FillMass(quantity);  //вызов функции(выделение памяти и заполнения массива)


    //1. Использую одно из средств IPC(неименованные каналы)
    for(int i = 0; i<quantity; i++){
		if(pipe(fd[i])==-1){
			perror("Can't open channel");
			exit(1);
		}
     }
    

    //2. Инициализирую многозадачность(параллельность процессов)
	for (unsigned short i = 0; i < quantity; i++){
	 pid[i] = fork();
      if (pid[i] == 0){     //потомок стартует
        close(fd[i][0]);    //потомок закрывает конец канала для чтения
        CheckSum = ProcessingFiles(MassPointFile, i);
        printf("\nChildPid%d: \n", getpid());
        printf("File%d --> Check sum = %d bytes\n\n", i + 1, CheckSum);
        write(fd[i][1], &CheckSum, sizeof(int));
        exit(0);
    }
        else if (-1 == pid[i]){
        	perror("Can't create procrss");
        }
     } 

     
     freeMemory(MassPointFile); //очищаю память под строки
     do{    //3. Родительский процесс стартует
     for (unsigned short i = 0; i < quantity; i++){
     	pidoff = waitpid(pid[i], &status, WUNTRACED | WCONTINUED);
     	if (pidoff == pid[i]){
     		close(fd[i][1]);    //родитель закрывает конец канала для записи
     		    int CheckSum;
     		    read(fd[i][0], &CheckSum, sizeof(int));
                count++;
                resultBuffer[i] = CheckSum; //пишу в массив значение перемнной Cheksum                                     
                }
     	}
       } while( count != quantity);



printf("ParentPid%d: Finishing...\nTotal check sum = %d bytes\n", getpid(), TotalCheckSum(resultBuffer)); 
free(resultBuffer); //очищаю память по результат
return 0;
}


//функция кладет названия файлов в массивы 
char** FillMass(unsigned short quantity){
 	char **MassPointFile = malloc(quantity * sizeof(unsigned short*));
	for (unsigned short i = 0; i < quantity; i++){  
		char buffer[MAX_LEN];
		printf("File %hi: ", i + 1);
		scanf("%s", buffer);
	    MassPointFile[i] = malloc(strlen(buffer) * sizeof(char));
	    strcpy(MassPointFile[i], buffer);
	}
	return MassPointFile;
 }



//функция считает контрольную сумму отдельнго файла
int ProcessingFiles(char **MassPointFile, unsigned short i){
FILE *file = fopen(MassPointFile[i], "r");
if(file == NULL){
      	 printf("Ошибка. Невозможно открыть файл.\n");
       	 exit(1);
} 
char symbol;
int CheckSum = 0; 
while( ( symbol = fgetc(file) ) != EOF ){
	CheckSum +=sizeof(symbol);
}
    fclose(file);
    return CheckSum;
}



//функция считает общую контрльную сумму файла
int TotalCheckSum(int *resultBuffer){
	int TotalSUM = 0;
	for (unsigned short i = 0; i < quantity; i++)
	{
		TotalSUM += resultBuffer[i];
	}
   return TotalSUM;
}

void freeMemory(char **MassPointFile){
	for (unsigned char i = 0; i < quantity; i++)
	{
		free(MassPointFile[i]);
	}
   free(MassPointFile);
   return ;
}