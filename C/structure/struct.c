/*-----------------------------------------------------------------------------------

Surname
Date of Birth
Phone
The sum of debt
All records with the sum of debt not equal to 0, placed at the beginning of the array

------------------------------------------------------------------------------------*/
#include "stdio.h"
#include "malloc.h"
#include <stdlib.h>
#include <string.h>

#define MAX_SYMBOL 100 



struct MySQL
{
	char familyName[MAX_SYMBOL];
	char dateofBirth[MAX_SYMBOL];
	int mobilePhone;
	int theSumofDebt;
}__attribute__((packed));

typedef struct MySQL mixType;

void printStruct (mixType **p, int quantity)
{
	int i ;
	printf("\n%45s\n", "Sum of dept:");
	for (i = 0; i < quantity; ++i)
	{
		printf("%s:%d rub\n", p[i]->familyName, p[i]->theSumofDebt);
	}
	
	return ;
}


void sortMySQL (mixType **p, int quantity)
{
	 
 int i, j, tmp = 0;

 for (i = 0; i < quantity; ++i)
  {  
     for (j = i + 1; j < quantity; ++j)
      { 
				if((p[j]->theSumofDebt) > (p[i]->theSumofDebt))
			{
					tmp = p[j];
                    p[j] = p[i];
                    p[i] = tmp;
            }
      }
    	
}
return ;

}

void readStruct (mixType *p)
{
	    printf("Enter family name: ");
        scanf("%s", p->familyName);
        
        printf("Enter your date of birth: ");
        scanf("%s", p->dateofBirth);
        
        printf("Enter the number of Phone: ");
        scanf("%d", &p->mobilePhone);
        
        printf("Enter the sum of debt: ");
        scanf("%d", &p->theSumofDebt);
        
        return ; 
}


void freeStruct(mixType **p, int quantity)
{
	int i;
	for (i = 0; i < quantity; ++i)
	{
		free(p[i]);
	}

	free(p);
	return ;
}


int main (void)

{
   mixType MySQL;
   mixType **p = NULL; 
   int i, quantity;
   printf("Enter tne quantity of users = ");
   scanf("%d", &quantity);
   p = malloc(sizeof(mixType**) * quantity);
   for (i = 0; i < quantity; ++i)
	{  
		p[i] = malloc(sizeof(mixType*));
		readStruct(p[i]);
    }
    sortMySQL(p, quantity);
    printStruct(p, quantity);
    freeStruct(p, quantity);
    return 0;
}
